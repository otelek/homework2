class AddArticlePriceToOrderLines < ActiveRecord::Migration
  def change
    add_column :order_lines, :article_price, :decimal
    add_column :order_lines, :article_title, :string
  end
end
