class Order < ActiveRecord::Base
  attr_accessible :client_name, :shipping_address, :order_lines_attributes

  has_many :order_lines

  accepts_nested_attributes_for :order_lines, allow_destroy: true
end
